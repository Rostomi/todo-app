import React, { Fragment, Component } from "react";
import TodoItem from "./TodoItem";

export default class TodoList extends Component {
  render() {
    const { items, clearList, handleDelete, handleEdit } = this.props;
    return (
      <Fragment>
        <ul className='list-group my-5'>
          <h3 className='text-capitalize text-center text-light'>გააკეთე</h3>
          {items.map(item => {
            return (
              <TodoItem
                key={item.id}
                title={item.title}
                handleDelete={() => {
                  handleDelete(item.id);
                }}
                handleEdit={() => {
                  handleEdit(item.id);
                }}
              />
            );
          })}
          <button
            type='submit'
            className='btn btn-danger btn-block text-capitalize mt-5'
            onClick={clearList}
          >
            ყველაფრის წაშლა
          </button>
        </ul>
      </Fragment>
    );
  }
}
