import React, { Fragment, Component } from "react";

export default class TodoInput extends Component {
  render() {
    const { item, handleChange, handleSubmit, editItem } = this.props;
    return (
      <Fragment>
        <div className='card card-body my-3 bg-dark border-secondary'>
          <form onSubmit={handleSubmit}>
            <div className='input-group'>
              <div className='input-group-prepend'>
                <div className='input-group-text bg-primary text-white'>
                  <i className='fas fa-book'></i>
                </div>
              </div>
              <input
                type='text'
                className='form-control bg-secondary text-white'
                placeholder='მაგ: ბაღის მორწყვა'
                value={item}
                onChange={handleChange}
              />
            </div>
            <button
              type='submit'
              className={
                editItem
                  ? "btn btn-block btn-success mt-3"
                  : "btn btn-block btn-primary mt-3"
              }
            >
              {editItem ? "ჩასწორება" : "დამატება"}
            </button>
          </form>
        </div>
      </Fragment>
    );
  }
}
