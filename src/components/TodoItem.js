import React, { Fragment, Component } from "react";

export default class TodoItem extends Component {
  render() {
    const { title, handleDelete, handleEdit } = this.props;
    return (
      <Fragment>
        <li className='list-group-item d-flex flex-wrap justify-content-between my-2 bg-secondary '>
          <h6 className='text-white'>{title}</h6>
          <div className='todo-icon'>
            <span className='mx-2 text-warning' onClick={handleEdit}>
              <i className='fas fa-pen'></i>
            </span>
            <span className='mx-2 text-danger' onClick={handleDelete}>
              <i className='fas fa-trash'></i>
            </span>
          </div>
        </li>
      </Fragment>
    );
  }
}
