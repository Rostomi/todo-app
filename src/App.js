import React, { Fragment, Component } from "react";
import TodoInput from "./components/TodoInput";
import TodoList from "./components/TodoList";

import SimpleStorage from "react-simple-storage";
import "bootstrap/dist/css/bootstrap.min.css";
import { v4 as uuidv4 } from "uuid";

export default class App extends Component {
  state = {
    items: [],
    id: uuidv4(),
    item: "",
    editItem: false
  };

  handleChange = e => {
    this.setState({
      item: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();

    const newItem = {
      id: this.state.id,
      title: this.state.item
    };

    if (newItem.title !== "") {
      // console.log(newItem);
      const updatedItems = [...this.state.items, newItem];
      this.setState({
        items: updatedItems,
        item: "",
        id: uuidv4(),
        editItem: false
      });
    }
  };

  clearList = () => {
    this.setState({
      items: []
    });
  };

  handleDelete = id => {
    const filteredItems = this.state.items.filter(item => item.id !== id);

    this.setState({
      items: filteredItems
    });
  };

  handleEdit = id => {
    const filteredItems = this.state.items.filter(item => item.id !== id);

    const selectedItem = this.state.items.find(item => item.id === id);

    // console.log(selectedItem);

    this.setState({
      items: filteredItems,
      item: selectedItem.title,
      editItem: true,
      id: id
    });
  };

  render() {
    return (
      <Fragment>
        <SimpleStorage parent={this} />
        <div className='container'>
          <div className='row'>
            <div className='col-10 mx-auto col-md-8 mt-4'>
              <h3 className='text-capitalize text-center text-light'>
                დაამატე
              </h3>
              <TodoInput
                item={this.state.item}
                handleChange={this.handleChange}
                handleSubmit={this.handleSubmit}
                editItem={this.state.editItem}
              />
              <TodoList
                items={this.state.items}
                clearList={this.clearList}
                handleDelete={this.handleDelete}
                handleEdit={this.handleEdit}
              />
              <a
                href='https://rostomi-dev.github.io/'
                target='_blank'
                rel='noopener noreferrer'
                className='float-right'
              >
                by rostomi-dev
              </a>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}
